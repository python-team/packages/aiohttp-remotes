Source: aiohttp-remotes
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Adam Cecile <acecile@le-vert.net>
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 python3-all,
 python3-setuptools,
# From pyproject.toml
 python3-aiohttp (>= 3.6.3~),
 python3-typing-extensions (>= 3.7.4~),
# UnitTest
 python3-pytest <!nocheck>,
 python3-pytest-cov <!nocheck>,
 python3-pytest-asyncio <!nocheck>,
# From docs/conf.py
 python3-sphinx <!nodoc>,
 python3-alabaster <!nodoc>,
 python3-sphinxcontrib-asyncio <!nodoc>,
Standards-Version: 4.5.1
Homepage: https://github.com/aio-libs/aiohttp-remotes
Vcs-Browser: https://salsa.debian.org/python-team/packages/aiohttp-remotes
Vcs-Git: https://salsa.debian.org/python-team/packages/aiohttp-remotes.git
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: python3-aiohttp-remotes
Architecture: all
Depends:
 ${python3:Depends},
 ${misc:Depends},
Suggests: python-aiohttp-remotes-doc
Description: Set of useful tools for Python aiohttp.web server (Python 3)
 This library is a set of useful tools for aiohttp.web server.
 .
 The full list of tools is:
 .
  * AllowedHosts -- restrict a set of incoming connections to allowed hosts
    only
  * BasicAuth -- protect web application by basic auth authorization
  * Cloudflare -- make sure that web application is protected by CloudFlare
  * ForwardedRelaxed and ForwardedStrict -- process Forwarded HTTP header
    and modify corresponding scheme, host, remote attributes in strong
    secured and relaxed modes
  * Secure -- ensure that web application is handled by HTTPS (SSL/TLS)
    only, redirect plain HTTP to HTTPS automatically
  * XForwardedRelaxed and XForwardedStrict -- the same as ForwardedRelaxed
    and ForwardedStrict but process old-fashion X-Forwarded-* headers
    instead of new standard Forwarded
 .
 This package installs the library for Python 3.

Package: python-aiohttp-remotes-doc
Architecture: all
Section: doc
Depends:
 ${sphinxdoc:Depends},
 ${misc:Depends},
Description: Set of useful tools for Python aiohttp.web server (common documentation)
 This library is a set of useful tools for aiohttp.web server.
 .
 The full list of tools is:
 .
  * AllowedHosts -- restrict a set of incoming connections to allowed hosts
    only
  * BasicAuth -- protect web application by basic auth authorization
  * Cloudflare -- make sure that web application is protected by CloudFlare
  * ForwardedRelaxed and ForwardedStrict -- process Forwarded HTTP header
    and modify corresponding scheme, host, remote attributes in strong
    secured and relaxed modes
  * Secure -- ensure that web application is handled by HTTPS (SSL/TLS)
    only, redirect plain HTTP to HTTPS automatically
  * XForwardedRelaxed and XForwardedStrict -- the same as ForwardedRelaxed
    and ForwardedStrict but process old-fashion X-Forwarded-* headers
    instead of new standard Forwarded
 .
 This is the common documentation package.
